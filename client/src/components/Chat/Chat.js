import React, { useEffect, useState } from "react";
import styles from "./Chat.module.css";
import InputBubble from "../InputBubble/InputBubble";
import Messages from "../Messages/Messages";
import { useSelector, useDispatch } from "react-redux";
import { useLocation } from "react-router";
import queryString from "query-string";
import * as actionType from "../../store/actions/actionTypes";
import {
  initiateSocket,
  disconnectSocket,
  subscribeToChat,
  loadAllMessages,
} from "../../socket/Socket";

const Chat = (props) => {
  const [messages, setMessages] = useState([]);
  const location = useLocation();
  const dispatch = useDispatch();
  const selectedRoom = useSelector((state) => state.chat.chatRoom);

  const [message, setMessage] = useState({
    sender: "",
    text: "",
    chatRoom: {},
  });

  useEffect(() => {
    const { username } = queryString.parse(location.search);

    if (selectedRoom) {
      initiateSocket(selectedRoom, username);

      loadAllMessages((err, data) => {
        if (err) return;
        const chatHistory = data.map((message) => {
          return {
            chatRoom: { _id: message.chatRoomId },
            sender: message.user,
            text: message.content,
          };
        });
        setMessages(chatHistory);
      });
    }

    subscribeToChat((err, data) => {
      if (err) return;
      setMessages((oldMessages) => [...oldMessages, data]);

      if (data.sender !== username) {
        const newMessage = {
          arrived: true,
          room: data.chatRoom._id,
          sender: data.sender,
        };
        dispatch({ type: actionType.SET_NEW_MESSAGE, newMessage });
      }
    });

    return () => {
      disconnectSocket(selectedRoom, username);
    };
  }, [selectedRoom]);

 
  return (
    <div className={styles.chatArea}>
      <div id={styles.messagingArea} >
        {selectedRoom.members.length > 0 ? (
          <Messages messages={messages} selectedRoom={selectedRoom} />
        ) : (
          <p
            style={{
              color: "black",
              fontFamily: "Quicksand",
              padding: "40px",
            }}
          >
            Start a conversation by selecting a chat room from <b>chat rooms</b>
            .
          </p>
        )}
      </div>
      <footer>
        <InputBubble message={message} onSetMessage={setMessage} />
      </footer>
    </div>
  );
};

export default Chat;
