import React, { useState, useEffect } from "react";
import styles from "./InfoBar.module.css";
import logo from "../../resources/images/chat.svg";
import ToggleSwitch from "../../ui/ToggleSwitch/ToggleSwitch";
import { useSelector } from "react-redux";
import { LazyLoadImage } from "react-lazy-load-image-component";

const InfoBar = (props) => {
  const chatRoom = useSelector((state) => state.chat.chatRoom);
  const [color, setColor] = useState("");
  const [chatRoomNameColor, setchatRoomNameColor] = useState("");

  const theme = useSelector((state) => state.theme.toggleMode);

  useEffect(() => {
    if (theme === "dark") {
      setColor("black");
      setchatRoomNameColor("white");
    } else {
      setColor("#212121");
      setchatRoomNameColor("#212121");
    }
  }, [theme]);

  return (
    <div className={styles.container}>
      <div className={styles.nested}>
        <div id={styles.upper}>
          <img src={logo} alt="logo" style={{ width: "32px" }} />
          <span id={styles.gradientText}>messaging</span>
        </div>
        <div>
          <h1 style={{ color: color }}>chat rooms</h1>
        </div>
      </div>
      <div className={styles.central}>
        <div>
          {chatRoom.members.length > 0 ? (
            <LazyLoadImage
              id={styles.profileImg}
              src={"http://localhost:5000/api/image/" + chatRoom.image.image}
              alt="profileImg"
              width="58px"
              height="58px"
              effect="blur"
            />
          ) : null}
        </div>
        <div>
          <p id={styles.contactName} style={{ color: chatRoomNameColor }}>
            {chatRoom.name}
          </p>
        </div>
      </div>
      <div className={styles.nested}>
        <div id={styles.upper}>
          <ToggleSwitch />
        </div>
        <div>
          <h1 style={{ color: color }}>chat members</h1>
        </div>
      </div>
    </div>
  );
};

export default InfoBar;
