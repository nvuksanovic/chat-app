import React, {useEffect, useState} from "react";
import styles from "./ChatMembers.module.css";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import queryString from "query-string";

const ChatMembers = (props) => {
  const chatRoom = useSelector((state) => state.chat.chatRoom);
  const location = useLocation();
  const { username } = queryString.parse(location.search);
  const [color, setColor] = useState("");
  const theme = useSelector((state) => state.theme.toggleMode);

  useEffect(() => {
    theme === "dark" ? setColor("#362f56") : setColor("transparent");
  }, [theme]);

  return (
    <div className={styles.chatMembersTable}>
      <table style={{background: color}}>
        <tbody>
          { Object.entries(chatRoom).length !== 0
            ? chatRoom.members.map((mem, i) => (
                <tr key={i}>
                  <td>{mem === username ? "You" : mem}</td>
                </tr>
              ))
            : null}
        </tbody>
      </table>
    </div>
  );
};

export default ChatMembers;
