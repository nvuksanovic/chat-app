import React, { useState, useEffect } from "react";
import styles from "./InputBubble.module.css";
import { useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import sendIcon from "../../resources/images/send-mail.png";
import emoji from "../../resources/images/happy.png";
import sendIconDisabled from "../../resources/images/send-mail-disabled.png";
import queryString from "query-string";
import { sendMessage } from "../../socket/Socket";
import Picker from "emoji-picker-react";

const InputBubble = ({ message, onSetMessage }) => {
  const [username, setUsername] = useState("");
  const [disabled, setDisabled] = useState(true);
  const location = useLocation();
  const chatRoom = useSelector((state) => state.chat.chatRoom);
  const [showPicker, setShowPicker] = useState(false);
  const [color, setColor] = useState("");
  const theme = useSelector((state) => state.theme.toggleMode);

  useEffect(() => {
    theme === "dark" ? setColor("#362f56") : setColor("white");
  }, [theme]);

  const onEmojiClick = (event, emojiObject) => {
    onSetMessage((prevState) => ({
      text: prevState.text + emojiObject.emoji,
    }));
    setShowPicker(false);
  };

  useEffect(() => {
    const { username } = queryString.parse(location.search);
    setUsername(username);
  }, [location]);

  useEffect(() => {
    if (chatRoom) setDisabled(false);
  }, [chatRoom]);

  const messageHandler = (e) => {
    if (e.keyCode === 13) {
      send();
    }
  };

  const send = () => {
    if (message.text !== "") {
      onSetMessage({
        ...message,
        text: "",
        sender: "",
        chatRoom: "",
      });
      sendMessage(chatRoom, message);
    }
  };

  return (
    <div className={styles.inputBubble}>
      <div id={styles.inputBubble} style={{ backgroundColor: color }}>
        <input
          style={{ backgroundColor: color }}
          type="text"
          placeholder="write a message..."
          value={message.text}
          onKeyDown={messageHandler}
          onChange={(e) => {
            onSetMessage({
              ...message,
              text: e.target.value,
              sender: username,
              chatRoom: chatRoom,
            });
          }}
          onClick={() => setShowPicker(false)}
          disabled={disabled}
        ></input>

        <button type="submit" onClick={send} disabled={disabled}>
          <img
            src={disabled ? sendIconDisabled : sendIcon}
            alt="send-message-icon"
          />
        </button>
        <button type="submit" onClick={() => setShowPicker((val) => !val)}>
          <img src={emoji} alt="emoji-picker-icon" />
        </button>
      </div>
      <div id={styles.emojiPicker}>
        {showPicker && (
          <Picker pickerStyle={{ width: "100%" }} onEmojiClick={onEmojiClick} />
        )}
      </div>
    </div>
  );
};

export default InputBubble;
