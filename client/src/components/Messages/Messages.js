import { Fragment } from "react";
import MessageBubble from "./MessageBubble/MessageBubble";
import React, { useEffect, useRef } from "react";

const Messages = ({ messages, selectedRoom }) => {
  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(scrollToBottom, [messages]);
 
  return (
    <Fragment>
      { messages.map((message, i) => {
        if(message.chatRoom._id === selectedRoom._id)
          return (
            <div key={i}>
              <MessageBubble sender={message.sender} text={message.text} />
            </div>
          );
      })}
      <div ref={messagesEndRef} />
    </Fragment>
  );
};

export default Messages;
