import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import styles from "./MessageBubble.module.css";
import ReactEmoji from "react-emoji";
import { useSelector } from "react-redux";

const MessageBubble = ({ sender, text }) => {
  const [currentUser, setCurrentUser] = useState("");
  const { search } = useLocation();
  const [fontColor, setFontColor] = useState("");
  const theme = useSelector((state) => state.theme.toggleMode);

  useEffect(() => {
    const username = new URLSearchParams(search).get("username");
    setCurrentUser(username);
  }, [search, currentUser]);

  useEffect(() => {
    theme === "dark" ? setFontColor("white") : setFontColor("#3d3d3d");
  }, [theme]);

  return sender === currentUser ? (
    <div
      className={styles.messageBubble}
      style={{ justifyContent: "flex-start" }}
    >
      <span style={{ color: fontColor }}>{sender}:</span>
      <div id={styles.bubble}>
        <p>{ReactEmoji.emojify(text)}</p>
      </div>
    </div>
  ) : (
    <div
      className={styles.messageBubble}
      style={{ justifyContent: "flex-end" }}
    >
      <span style={{ color: fontColor }}>
        {sender}
        {sender ? ":" : ""}
      </span>
      <div id={styles.bubble}>
        <p>{ReactEmoji.emojify(text)}</p>
      </div>
    </div>
  );
};

export default MessageBubble;
