import React, { useEffect, useState } from "react";
import styles from "./ChatRoom.module.css";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useSelector } from "react-redux";

const ChatRoom = ({ src, selected, name, active }) => {
  const [color, setColor] = useState("");
  const theme = useSelector((state) => state.theme.toggleMode);

  useEffect(() => {
    theme === "dark" ? setColor("white") : setColor("black");
  }, [theme]);

  return (
    <li className={styles.chatRoom} onClick={selected}>
      <div>
        <LazyLoadImage src={src} width="50px" height="50px" effect="blur" />
      </div>
      <p className={active ? styles.active : null} style={{color: color}}>{name}</p>
    </li>
  );
};

export default ChatRoom;
