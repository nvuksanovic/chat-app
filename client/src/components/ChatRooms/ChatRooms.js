import React, { useEffect, useState } from "react";
import styles from "./ChatRooms.module.css";
import ChatRoom from "./ChatRoom/ChatRoom";
import Button from "../../ui/Button/Button";
import AddNewRoom from "./AddNewRoom/AddNewRoom";
import axios from "axios";
import { useLocation } from "react-router";
import queryString from "query-string";
import { useDispatch } from "react-redux";
import * as actionType from "../../store/actions/actionTypes";

const ChatRooms = (props) => {
  const [open, setDialogOpen] = useState(false);
  const [chatRooms, setChatRooms] = useState([]);
  const [selectedChatRoom, setSelectedChatRoom] = useState();
  const location = useLocation();
  const dispatch = useDispatch();
  const { username } = queryString.parse(location.search);

  const closeDialog = (isClosed) => {
    isClosed ? setDialogOpen(false) : setDialogOpen(true);
  };

  useEffect(() => {
    axios
      .get("http://localhost:5000/api/chatroom/" + username)
      .then((res) => {
        const responseData = res.data.map((chatRoom) => {
          return {
            _id: chatRoom._id,
            name: chatRoom.names.filter((name) => name.user === username).pop()
              .name,
            members: chatRoom.members,
            image: chatRoom.images.filter((image) => image.user === username).pop()
          };
        });

        setChatRooms(responseData);
      })
      .catch((error) => console.log(error.data));
  }, [location, username, chatRooms]);

  const selectChatRoomHandler = (ind) => {
    setSelectedChatRoom(ind);
    const chatRoom = chatRooms.find((cr, i) => i === ind);
    dispatch({ type: actionType.SET_CHAT_ROOM, chatRoom });
  };

  return (
    <div className={styles.leftDiv}>
      <div id={styles.listDiv}>
        <ul>
          {chatRooms.map((cr, i) => {
            return (
            <ChatRoom
              src={"http://localhost:5000/api/image/" + cr.image.image}
              key={i}
              alt={cr.name}
              name={cr.name}
              active={i === selectedChatRoom}
              selected={() => selectChatRoomHandler(i)}
            />
          )})}
        </ul>
      </div>
      <div id={styles.buttonDiv}>
        <Button name="add new chat" clicked={() => closeDialog()} />
      </div>
      {open ? <AddNewRoom closeDialog={closeDialog} /> : null}
    </div>
  );
};

export default ChatRooms;
