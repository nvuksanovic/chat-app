import React, { useState } from "react";
import styles from "./AddNewRoom.module.css";
import axios from "axios";
import { useLocation } from "react-router";
import queryString from "query-string";


const AddNewRoom = (props) => {
  const [newChatMember, setNewChatMember] = useState("");
  const [newChatRoom, setNewChatRoom] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const location = useLocation();

  const addNewChatRoomHandler = () => {
    if (newChatMember !== "" && newChatRoom !== "") {
      postNewChatData();
    } else setErrorMessage("Please fill all the boxes.");
  };

  const postNewChatData = () => {
    const {username} = queryString.parse(location.search);
    const chatData = {
      chatRoom: newChatRoom,
      newMember: newChatMember,
      user: username
    };

    axios
      .post("http://localhost:5000/api/chatroom", chatData)
      .then((res) => {
        props.closeDialog(true);
      })
      .catch((err) => {
        const errorMessage = err.response.data;
        setErrorMessage(errorMessage);
      });
  };

  return (
    <div className={styles.dialog}>
      <span onClick={props.closeDialog}> &times;</span>
      <input
        type="text"
        placeholder="new contact"
        onChange={(e) => {
          setNewChatMember(e.target.value);
        }}
      />
      <input
        type="text"
        placeholder="chat room name"
        onChange={(e) => {
          setNewChatRoom(e.target.value);
        }}
      />
      <button onClick={addNewChatRoomHandler}>add</button>
      <p>{errorMessage}</p>
    </div>
  );
};

export default AddNewRoom;
