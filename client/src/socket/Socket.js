import io from "socket.io-client";

let socket;

export const initiateSocket = (room, user) => {
  socket = io("http://localhost:5000");
  console.log("Conecting socket...");
  if (socket && room) socket.emit("join", room, user);
};

export const subscribeToChat = (cb) => {
  if (!socket) return (true);
  socket.on("chat", (msg) => {
    return cb(null, msg);
  });
};

export const loadAllMessages = (cb) => {
  if (!socket) return (true);
  socket.on("load all messages", (msgs) => {
    return cb(null, msgs);
  });
}

export const sendMessage = (room, message) => {
  if (socket) socket.emit("chat", { message, room });
};

export const disconnectSocket = (room, user) => {
  console.log("Disconnecting socket...");
  if(socket){
    socket.disconnect("disconnect", room, user);
  }
};
