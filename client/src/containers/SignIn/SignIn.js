import React, { useState } from "react";
import styles from "./SignIn.module.css";
import Button from "../../ui/Button/Button";
import logo from "../../resources/images/chat.svg";
import { NavLink, useHistory } from "react-router-dom";
import ErrorComponent from "../../ui/ErrorComponent/ErrorComponent";
import axios from "axios";
import NotificationBubble from "../../ui/NotificationBubble/NotificationBubble";


const SignIn = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [showNotif, setNotif] = useState(false);
  const history = useHistory();


  const handleSubmit = (event) => {
    if (email === "" || password === "") {
      event.preventDefault();
      setErrorMessage("Please fill all the boxes.");
    } else postLoginUserData();
  };

  const postLoginUserData = () => {
    const userData = {
      email: email,
      password: password,
    };

    axios
      .post("http://localhost:5000/api/login", userData)
      .then((res) => {
        if (res.status === 200) {
          history.push("/chat?username=" + res.data);
        }
      })
      .catch((err) => {
        const errorMessage = err.response.data;
        setErrorMessage(errorMessage);
      });
  };

  const handleResetPassword = () => {
    axios
      .post("http://localhost:5000/api/reset", { email: email })
      .then((res) => {
        console.log(res);
        setNotif(true);
      })
      .catch((err) => {
        const errorMessage = err.response.data;
        console.log(errorMessage);
      });
  };

  return (
    <div className={styles.signIn}>
      <div id={styles.logoArea}>
        <img src={logo} alt="logo" />
        <span id={styles.gradientText}>messaging</span>
      </div>

      <input
        type="text"
        placeholder="email"
        onChange={(event) => setEmail(event.target.value)}
      />
      <input
        type="password"
        placeholder="password"
        onChange={(event) => setPassword(event.target.value)}
      />

      <div id={styles.button}>
        <Button name="submit" clicked={handleSubmit} />
      </div>
      {errorMessage !== "" ? <ErrorComponent message={errorMessage} /> : null}
      {email !== "" && errorMessage === "Wrong password! Please try again." ? (
        <p onClick={handleResetPassword}>Forgot password?</p>
      ) : null}
      <p>
        You don’t have an account? Register
        <NavLink to={"/register"}> here</NavLink>.
      </p>
      {showNotif ? (
        <NotificationBubble text="Check your email. Reset link is sent." />
      ) : null}
    </div>
  );
};

export default SignIn;
