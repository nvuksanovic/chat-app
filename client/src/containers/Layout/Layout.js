import React, { useEffect, useState } from "react";
import ChatRooms from "../../components/ChatRooms/ChatRooms";
import ChatMembers from "../../components/ChatMembers/ChatMembers";
import Chat from "../../components/Chat/Chat";
import styles from "./Layout.module.css";
import Nav from "../Nav/Nav";
import { useSelector } from "react-redux";
import InfoBar from "../../components/InfoBar/InfoBar";

const Layout = ({ location }) => {
  const chatRoom = useSelector((state) => state.chat.chatRoom);
  const theme = useSelector((state) => state.theme.toggleMode);
  const [background, setBackground] = useState("");

  useEffect(() => {
    theme === "dark"
      ? setBackground("#080927b6")
      : setBackground("rgba(46, 46, 46, 0.15)");
  }, [theme]);

  return (
    <div>
      <Nav />
      <div className={styles.container} style={{ background: background }}>
        <InfoBar params={location.search} />
        <div id={styles.centralContent}>
          <div id={styles.leftDiv}>
            <ChatRooms />
          </div>
          <div id={styles.centralDiv}>
            <Chat />
          </div>
          <div id={styles.rightDiv}>
            {chatRoom !== "" ? <ChatMembers /> : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Layout;
