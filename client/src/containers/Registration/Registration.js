import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import styles from "../SignIn/SignIn.module.css";
import Button from "../../ui/Button/Button";
import logo from "../../resources/images/chat.svg";
import ProfileImagePicker from "../../ui/ProfileImagePicker/ProfileImagePicker";
import ErrorComponent from "../../ui/ErrorComponent/ErrorComponent";
import axios from "axios";
import defaultUserImg from "../../resources/images/astronaut.png";

const Registration = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [profileImage, setProfileImage] = useState(defaultUserImg);
  const [imageFile, setImageFile] = useState({});
  const history = useHistory();

  const handleSubmit = (event) => {
    if (username === "" || password === "" || email === "") {
      event.preventDefault();
      setErrorMessage("Please fill all the boxes.");
    } else postRegisterUserData();
  };

  const postRegisterUserData = () => {
    const userData = {
      email: email,
      name: username,
      password: password,
      profileImage: imageFile.name,
    };

    const formData = new FormData();
    formData.append("file", imageFile, imageFile.name);
    const config = {
      headers: { "Content-Type": "multipart/form-data", "x-Trigger": "CORS" },
    };
    const url = "http://localhost:5000/api/image/upload";

    axios
      .post(url, formData, config)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });

    axios
      .post("http://localhost:5000/api/register", userData, {
        headers: { "x-Trigger": "CORS" },
      })
      .then((res) => {
        console.log(res);
        if (res.status === 200) history.push("/chat?username=" + username);
      })
      .catch((err) => {
        const errorMessage = err.response.data;
        setErrorMessage(errorMessage);
      });
  };

  return (
    <div className={styles.signIn}>
      <div id={styles.logoArea}>
        <img src={logo} alt="logo" />
        <span id={styles.gradientText}>messaging</span>
      </div>
      <input
        type="email"
        placeholder="type your email address"
        onChange={(event) => setEmail(event.target.value)}
      />
      <input
        type="text"
        placeholder="create username"
        onChange={(event) => setUsername(event.target.value)}
      />
      <input
        type="password"
        placeholder="create password"
        onChange={(event) => setPassword(event.target.value)}
      />
      <ProfileImagePicker
        profileImage={profileImage}
        setProfileImage={setProfileImage}
        setImageFile={setImageFile}
      />
      <div id={styles.button}>
        <Button name="submit" clicked={handleSubmit} />
      </div>
      {errorMessage !== "" ? <ErrorComponent message={errorMessage} /> : null}
    </div>
  );
};

export default Registration;
