import React, { useState } from "react";
import styles from "./ResetPassword.module.css";
import Button from "../../ui/Button/Button";
import logo from "../../resources/images/chat.svg";
import { useHistory, useRouteMatch } from "react-router-dom";
import axios from "axios";

const ResetPassword = (props) => {
  const [password, setPassword] = useState("");
  const [confirmedPassword, setConfirmedPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const match = useRouteMatch();
  const history = useHistory();

  const handleSubmit = (event) => {
    if (password === "" || confirmedPassword === "") {
      event.preventDefault();
      setErrorMessage("Please fill all the boxes.");
    } else {
      if(password === confirmedPassword)
        postResetData();
      else
        setErrorMessage("Passwords don't match!");
    }
  };

  const postResetData = () => {
    const userData = {
      password: password,
      passwordToken: match.params.token
    };

    axios
      .post("http://localhost:5000/api/new-password", userData)
      .then((res) => {
        console.log(res);
        if (res.status === 200) history.push("/chat?username=" + res.data);
      })
      .catch((err) => {
        const errorMessage = err.response.data;
        setErrorMessage(errorMessage);
      });
  };

  return (
    <div className={styles.signIn}>
      <div id={styles.logoArea}>
        <img src={logo} alt="logo" />
        <span id={styles.gradientText}>messaging</span>
      </div>
    
      <input
        type="password"
        placeholder="type new password"
        onChange={(event) => setPassword(event.target.value)}
      />
      <input
        type="password"
        placeholder="confirm password"
        onChange={(event) => setConfirmedPassword(event.target.value)}
      />
      <div id={styles.button}>
        <Button name="reset" clicked={handleSubmit} />
      </div>
      {errorMessage}
      <p>
        Password should contain minimum 5 <br />
        alphanumeric characters.
      </p>
    </div>
  );
};

export default ResetPassword;
