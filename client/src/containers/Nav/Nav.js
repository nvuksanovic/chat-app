import React, { useState } from "react";
import styles from "./Nav.module.css";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as actionType from "../../store/actions/actionTypes";
import NotificationBubble from "../../ui/NotificationBubble/NotificationBubble";

const Nav = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [notificationText, setNotificationText] = useState("");
  const newMessageIndicator = useSelector(
    (state) => state.chat.newMessageIndicator
  );

  const handleClick = () => {
    axios
      .post("http://localhost:5000/api/logout")
      .then((res) => {
        console.log(res);
        dispatch({ type: actionType.RESET_CHAT_ROOM });
        if (res.status === 200) history.push("/");
      })
      .catch((err) => {
        const errorMessage = err.response.data;
        console.log(errorMessage);
      });
  };

  useEffect(() => {
    if (newMessageIndicator.arrived) {
      setNotificationText(
        "You have new message from " + newMessageIndicator.sender + "."
      );
      setTimeout(() => {
        setNotificationText("");
        dispatch({ type: actionType.RESET_NEW_MESSAGE });
      }, 10000);
    }
  }, [newMessageIndicator, dispatch]);

  return (
    <div className={styles.nav}>
      <button onClick={handleClick}>sign out</button>
      {notificationText ? (
        <div id={styles.notification}>
          <NotificationBubble text={notificationText} />
        </div>
      ) : null}
    </div>
  );
};

export default Nav;
