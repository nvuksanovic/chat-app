import * as actionTypes from '../actions/actionTypes';

const initialState = {
    errorMessage: '',
    loading: false
}

const authentification = (state=initialState, action) => {
    switch(action.type){
      
        case actionTypes.USER_REGISTRATION_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null
            }
        
        case actionTypes.USER_REGISTRATION_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        
        case actionTypes.USER_REGISTRATION_START:
            return {
                ...state,
                loading: true
            }
            
            case actionTypes.USER_SIGNIN_START:
                return {
                    ...state,
                    loading: true
                }
            
            case actionTypes.USER_SIGNIN_FAIL:
                return {
                    ...state,
                    loading: false,
                    errorMessage: action.error
                }
            
            case actionTypes.USER_SIGNIN_SUCCESS:
                return {
                    ...state,
                    loading: false,
                    errorMessage: null
                }
            
            case actionTypes.USER_SIGNOUT_FAIL:
                return {
                    ...state,
                    errorMessage: action.error,
                    loading: false
                }
            
            case actionTypes.USER_SIGNOUT_START:
                return {
                    ...state,
                    loading: true
                }

            case actionTypes.USER_SIGNOUT_SUCCESS:
                return {
                    ...state,
                    loading: false,
                    errorMessage: null
                } 

        default:
            return state;
    }
}

export default authentification;