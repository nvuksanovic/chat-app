import * as actionTypes from "../actions/actionTypes";

const initialState = {
  toggleMode: "",
};

const chat = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_THEME_MODE:
      return {
        ...state,
        toggleMode: action.toggleMode,
      };

    default:
      return state;
  }
};

export default chat;
