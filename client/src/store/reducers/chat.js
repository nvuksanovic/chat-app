import * as actionTypes from "../actions/actionTypes";

const initialState = {
  chatRoom: { _id: "", name: "", members: [] },
  newMessageIndicator: { arrived: false, room: "", from: "" },
};

const chat = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_CHAT_ROOM:
      return {
        ...state,
        chatRoom: action.chatRoom,
      };

    case actionTypes.RESET_CHAT_ROOM:
      return {
        ...state,
        chatRoom: {},
      };

    case actionTypes.SET_NEW_MESSAGE:
      return {
        ...state,
        newMessageIndicator: action.newMessage,
      };

    case actionTypes.RESET_NEW_MESSAGE:
      return {
        ...state,
        newMessageIndicator: { arrived: false, room: "", from: "" },
      };

    default:
      return state;
  }
};

export default chat;
