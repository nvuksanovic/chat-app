import * as actionTypes from './actionTypes';
import axios from 'axios';


export const userRegistrationSuccess = () => {
    
    return {
        type: actionTypes.USER_REGISTRATION_SUCCESS
    }
}


export const userRegistrationFail = (error) => {
    return {
        type: actionTypes.USER_REGISTRATION_FAIL,
        error: error
    }
}


export const userRegistrationRequest = (username, email, password) => {
    return dispatch => {
        
        const userData = {
            username: username,
            email: email,
            password: password
        }
        
        let url = 'http://localhost:5000/api/register';
        
        axios.post(url, userData).then(response => {
            dispatch( userRegisterSuccess() );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( userRegisterFail(errorMessage) );
        });
    }
}

//LOGIN

export const userLoginSuccess = (token, userId, isAdmin) => {
    return {
        type: actionTypes.USER_LOGIN_SUCCESS,
        idToken: token,
        userId: userId,
        isAdmin: isAdmin
    }
}


export const userLoginFail = (error) => {
    return {
        type: actionTypes.USER_LOGIN_FAIL,
        error: error
    }
}


export const userLoginRequest = (email, password) => {
    return dispatch => {
        
        const authData = {
            email: email,
            password: password
        }
        
        let url = 'http://localhost:5000/api/user/login';
        
        const { data } = axios.post(url, authData).then(response => {
            Cookie.set('userInfo', JSON.stringify(data));
            dispatch( userLoginSuccess(response.data.token, response.data.id, response.data.isAdmin) );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( userLoginFail(errorMessage) );
        });
    }
}

export const userLogOutSuccess = () => {
    return {
        type: actionTypes.USER_LOGOUT_SUCCESS
    }
}

export const userLogOut = () => {
    return dispatch => {
        Cookie.remove("userInfo");
        dispatch( userLogOutSuccess() );
    }
}

export const getUserDataStart = () => {
    return {
        type: actionTypes.GET_USER_DATA_START,
    }
}

export const getUserDataSuccess = (userData) => {
    return {
        type: actionTypes.GET_USER_DATA_SUCCESS,
        userData: userData
    }
}

export const getUserDataFail = (error) => {
    return {
        type: actionTypes.GET_USER_DATA_FAIL,
        error: error
    }
}

export const getUserData = (token) => {
    return dispatch => {

        dispatch( getUserDataStart() );

        let url = 'http://localhost:5000/api/user/' + token;
        
        axios.get(url, {
            headers: {
              Authorization: ' Bearer ' + token
            }}).then(response => {
            dispatch( getUserDataSuccess(response.data) );
        }).catch(error => {
            const errorMessage = error.response.data;
            dispatch( getUserDataFail(errorMessage) );
        });
        
    }
}