import "./App.css";
import Layout from "./containers/Layout/Layout";
import SignIn from "./containers/SignIn/SignIn";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Registration from "./containers/Registration/Registration";
import ResetPassword from "./containers/ResetPassword/ResetPassword";
import PageNotFound from "./ui/PageNotFound/PageNotFound";
import { useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import backgroundImageLight from "./resources/images/background.jpg";
import backgroundImageDark from "./resources/images/background-dark.jpg";

function App() {
  const theme = useSelector((state) => state.theme.toggleMode);
  const [background, setBackground] = useState();

  useEffect(() => {
    theme === "dark"
      ? setBackground(backgroundImageDark)
      : setBackground(backgroundImageLight);
  }, [theme]);

  return (
    <Router>
      <div className="App" style={{backgroundImage: `url(${background})`}}>
        <header className="App-header">
            <Switch>
              <Route path="/chat" component={Layout} />
              <Route path="/register" component={Registration} />
              <Route path="/reset-password/:token" component={ResetPassword} />
              <Route path="/" exact component={SignIn} />
              <Route component={PageNotFound} />
            </Switch>
        </header>
      </div>
    </Router>
  );
}

export default App;
