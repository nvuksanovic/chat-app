import React, { useRef } from "react";
import styles from "./ProfileImagePicker.module.css";

const ProfileImagePicker = ({ profileImage, setProfileImage, setImageFile }) => {
  const hiddenFileInput = useRef(null);

  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };

  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    setImageFile(fileUploaded);
    setProfileImage(URL.createObjectURL(fileUploaded));
  };


  return (
    <div className={styles.container}>
      <div id={styles.image}>
        <img src={profileImage} alt="userProfile" />
        <p onClick={handleClick}>change image</p>
        <input
          type="file"
          ref={hiddenFileInput}
          onChange={handleChange}
          style={{ display: "none" }}
        />
      </div>
    </div>
  );
};

export default ProfileImagePicker;
