import React from "react";
import astronaut from "../../resources/images/astronaut-lost.png";

const pageNotFound = () => (
  <div>
    <h1
      style={{
        fontFamily: "Quicksand",
        color: "#2e2e2e",
        fontSize: "23px",
        textTransform: "capitalize",
        padding: "5px"
      }}
    >
      Page not found!
    </h1>
    <img src={astronaut} alt="lost astronaut" style={{width: "120px"}}/>
  </div>
);

export default pageNotFound;
