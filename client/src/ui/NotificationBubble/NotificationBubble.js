import React from "react";
import style from "./NotificationBubble.module.css";
import notificationIcon from '../../resources/images/notifications.png';

const notificationBubble = (props) => (
  <div className={style.notification}>
    <p> <img src={notificationIcon} alt="Notification icon" /> {props.text} </p>
  </div>
);

export default notificationBubble;
