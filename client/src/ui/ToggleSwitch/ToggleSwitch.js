import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import * as actionType from "../../store/actions/actionTypes";
import { setTheme } from "../../utils/theme";
import "./ToggleSwitch.scss";

const ToggleSwitch = (props) => {
  const [toggleMode, setToggleMode] = useState("");
  let theme = localStorage.getItem("theme");
  const dispatch = useDispatch();
  const [color, setColor] = useState("");

  const toggleHandler = () => {
    if (localStorage.getItem("theme") === "theme-dark") {
      setTheme("theme-light");
      setToggleMode("light");
    } else {
      setTheme("theme-dark");
      setToggleMode("dark");
    }
  };

  useEffect(() => {
    if (localStorage.getItem("theme") === "theme-dark") {
      setToggleMode("dark");
    } else if (localStorage.getItem("theme") === "theme-light") {
      setToggleMode("light");
    }
  }, [theme]);

  useEffect(() => {
    toggleMode === "dark" ? setColor("white") : setColor("black");
    dispatch({ type: actionType.SET_THEME_MODE, toggleMode });
  });

  return (
    <div className="div">
      <span id="mode" style={{ color: color }}>
        dark mode {toggleMode === "dark" ? "on" : "off"}
      </span>
      <label className="toggle-control">
          <input type="checkbox"  onChange={toggleHandler}></input>
        {toggleMode === "dark" ? (
          <span className="control" style={{ background: "#c8b8fc" }} />
        ) : (
          <span className="control" style={{ background: "transparent" }} />
        )}
      </label>
    </div>
  );
};

export default ToggleSwitch;
