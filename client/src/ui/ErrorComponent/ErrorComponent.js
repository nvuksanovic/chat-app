import React from 'react';
import styles from './ErrorComponent.module.css';

const errorComponent = (props) => (
    <div className={styles.error}>
        <p>{props.message}</p>
    </div>
)

export default errorComponent;