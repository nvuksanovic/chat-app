import React from "react";
import "./Button.scss";

const button = (props) => (
  <div className="div" >
    <button className="btn block-cube block-cube-hover" onClick={props.clicked} >
      <div className="bg-top">
        <div className="bg-inner"></div>
      </div>
      <div className="bg-right">
        <div className="bg-inner"></div>
      </div>
      <div className="bg">
        <div className="bg-inner"></div>
      </div>
      <div className="text">{props.name}</div>
    </button>
  </div>
);

export default button;

/* <button className={styles.button} onClick={props.clicked}>
    <img src={props.src} alt={props.alt} /> {props.title}
  </button>*/
