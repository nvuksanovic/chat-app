const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const chatRoomSchema = new Schema({
  names: {
    type: Array,
    required: true,
  },
  members: {
    type: Array,
    required: true,
  },
  images: {
    type: Array
  }

});

module.exports = mongoose.model("ChatRoom", chatRoomSchema);
