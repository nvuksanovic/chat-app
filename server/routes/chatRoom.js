const express = require("express");

const chatroomController = require("../controllers/chatRoom");

const router = express.Router();

router.post("/", chatroomController.postChatRoom);
router.get("/:username", chatroomController.getChatRoomsOfUser);
router.get("/get-id/:room", chatroomController.getChatRoomId);
router.get("/get-profile-images/:username", chatroomController.getProfileImagesInChatroom);

module.exports = router;
