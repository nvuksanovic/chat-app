const express = require("express");

const messageController = require("../controllers/message");

const router = express.Router();

router.post("/", messageController.postMessage);
router.get("/:chatRoomId", messageController.getMessagesFromChatRoom);


module.exports = router;
