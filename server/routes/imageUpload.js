const express = require("express");
const imageUploadController = require("../controllers/imageUpload");
const router = express.Router();
const upload = require("./../middleware/upload");

router.post("/upload", upload.single("file"), imageUploadController.postProfileImage);
//router.get("/:fileName", imageUploadController.getProfileImage);


module.exports = router;
