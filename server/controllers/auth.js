const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const sendgridTransport = require("nodemailer-sendgrid-transport");
const crypto = require("crypto");
const { validationResult } = require("express-validator");

const User = require("../models/user");

const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_key:
        "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",
    },
  })
);

exports.postRegister = (req, res, next) => {
  const email = req.body.email;
  const name = req.body.name;
  const password = req.body.password;
  const profileImage = req.body.profileImage;

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors.array());
    return res.status(422).send(errors.array()[0].msg);
  }

  User.findOne({ email: email })
    .then((userDoc) => {
      if (userDoc) {
       
        return res.status(400).send("Email already exists");
      }
      return bcrypt
        .hash(password, 12)
        .then((hashedPassword) => {
          const user = new User({
            email: email,
            password: hashedPassword,
            name: name,
            profileImage: profileImage
          });
          return user.save();
        })
        .then((result) => {
          res.status(200).send("Successfully registered!");
          transporter.sendMail({
            to: email,
            from: "nvuksanovic5@gmail.com",
            subject: "Welcome aboard",
            html:
              "<h1>You have successfully registered!</h1> </br>" +
              "<p> We are excited to have you get started. First you need to confirm  your </br> account." +
              " Please press the button below </p>" +
              "<button>CONFIRM</button>",
          });
        })
        .catch((err) => console.log(err));
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;

  User.findOne({ email: email }).then((userDoc) => {
    if (!userDoc)
      return res
        .status(400)
        .send("Email doesn't exist. Register first please.");

    bcrypt
      .compare(password, userDoc.password)
      .then((doMatch) => {
        if (doMatch) {
          req.session.isLoggedIn = true;
          req.session.user = userDoc;
          return req.session.save((err) => {
            console.log(err);
            res.status(200).send(userDoc.name);
          });
        }
        res.status(400).send("Wrong password! Please try again.");
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

exports.postLogout = (req, res, next) => {
  req.session.destroy((err) => {
    console.log(err);
    res.send("Logged out");
  });
};

exports.postReset = (req, res, next) => {
  const email = req.body.email;
  crypto.randomBytes(32, (err, buffer) => {
    if (err) {
      console.log(err);
    }
    const token = buffer.toString("hex");
    User.findOne({ email: email })
      .then((user) => {
        if (!user) {
          return res.status(400).send("No account with that email has found.");
        }
        user.resetToken = token;
        user.resetTokenExpiration = Date.now() + 360000;
        return user.save();
      })
      .then((result) => {
        transporter.sendMail({
          to: email,
          from: "nvuksanovic5@gmail.com",
          subject: "Password reset",
          html: `<p>You have requested a password reset. Click this <a href="http://localhost:3000/reset-password/${token}">
          link</a> to set a new password.</p>`,
        });
        return res.send("Reset link should be sent.");
      })
      .catch((err) => console.log(err));
  });
};

exports.getNewPassword = (req, res, next) => {
  const token = req.params.token;
  User.findOne({
    resetToken: token,
    resetTokenExpiration: { $gt: Date.now() },
  }).then((user) => {
    if (!user) {
      return res.status(400).send("Something is wrong!");
    }
    return res.send("Please create new password.");
  });
};

exports.postNewPassword = (req, res, next) => {
  const newPassword = req.body.password;
  const passwordToken = req.body.passwordToken;
  let user;

  User.findOne({
    resetToken: passwordToken,
    resetTokenExpiration: { $gt: Date.now() },
  })
    .then((result) => {
      if (!result) return res.send("User doesn't exist.");
      user = result;
      return bcrypt.hash(newPassword, 12);
    })
    .then((hashedPassword) => {
      user.password = hashedPassword;
      user.resetToken = null;
      user.resetTokenExpiration = undefined;
      return user.save();
    })
    .then((result) => {
      return res
        .status(200)
        .send(user.name);
    })
    .catch((err) => {
      console.log(err);
    });
};
