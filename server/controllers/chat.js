const Message = require("../models/message");

module.exports = (io) => {
  io.on("connection", (socket) => {
    console.log(`Connected: ${socket.id}`);

    socket.on("disconnect", (room, user) => {
      console.log(`Disconnected: ${socket.id}`);
     
    });

    socket.on("join", async (room, user) => {
      console.log(`Socket ${socket.id} joining ${room._id}`);
      socket.join(room._id);

      const messages = await Message.find({ chatRoomId: room._id })
      .sort({ createdAt: -1 })
      .limit(10);

      socket.emit("load all messages", messages.reverse());
      

      /* 
      const newUser = room.members.find((member) => member === user); //wtf
      socket.broadcast.to(room._id).emit("chat", {
        sender: "messaging",
        text: `User ${newUser} has joined the chat.`,
        chatRoom: room,
      });
      */
    });

    socket.on("chat", async (data) => {
      //console.log(`msg: ${data.message.text}, room: ${data.room._id}`);
      let message = {
        content: data.message.text,
        chatRoomId: data.room._id,
        user: data.message.sender,
      };

      const newMessage = new Message(message);

      try {
        const savedMessage = await newMessage.save();
        io.emit("chat", data.message); //io.to(data.room._id).emit("chat", data.message);
      } catch (err) {
        console.log(`error: ${err.message}`);
      }
    });
  });
};
