const User = require("../models/user");
const ChatRoom = require("../models/chatRoom");

exports.postChatRoom = async (req, res, next) => {
  const roomName = req.body.chatRoom;
  const newMember = req.body.newMember;
  const sender = req.body.user;

  const senderExists = await User.findOne({ name: sender });
  const newMemberExists = await User.findOne({ name: newMember });

  if (!senderExists) res.status(400).send("User doesn't exist!");
  if (!newMemberExists)
    res
      .status(400)
      .send(
        "Username of the user you want to create a chat room with doesn't exist."
      );
  if (newMember === sender)
    res.status(400).send("You cannot create chat room with yourself.");

  if (senderExists && newMemberExists) {
    const chatRoomExists = await ChatRoom.findOne({
      members: [sender, newMember],
    });
    if (chatRoomExists) {
      return res.status(400).send("This chat room already exists.");
    }

    const newChatRoom = new ChatRoom({
      names: [
        { user: sender, name: roomName },
        { user: newMember, name: sender },
      ],
      members: [sender, newMember],
      images: [
        { user: sender, image: newMemberExists.profileImage },
        { user: newMember, image: senderExists.profileImage },
      ],
    });

    try {
      const savedChatRoom = await newChatRoom.save();
      res.status(200).json(savedChatRoom);
    } catch (err) {
      res.status(500).json(err);
    }
  }
};

exports.getChatRoomsOfUser = async (req, res, next) => {
  const username = req.params.username;

  const usernameExists = await User.findOne({ name: username });

  if (!usernameExists) res.status(400).send("User doesn't exist!");
  else {
    const result = await ChatRoom.find({ members: { $in: [username] } });
    if (result) {
      return res.status(200).json(result);
    } else
      return res.status(400).send("There are no chat rooms for this user.");
  }
};

exports.getProfileImagesInChatroom = async (req, res, next) => {
  const username = req.params.username;
  const usersChatRooms = await ChatRoom.find({ members: { $in: [username] } });
  if (!usersChatRooms) {
    return res.status(400).send("User has no chat room!");
  }
  const members = usersChatRooms.map((cr) => cr.members);
  const names = members.map((members) =>
    members.filter((member) => member !== username).pop()
  );

  const users = await User.find({ name: { $in: names } });
  if (!users) {
    return res.status(400).send("User has no chat room!");
  }
  const images = users.map((user) => {
    return user.profileImage;
  });
  return res.status(200).send(images);
};

exports.getChatRoomId = async (req, res, next) => {
  const roomName = req.params.room;
  const user = req.body.user;

  const roomExists = await ChatRoom.findOne({
    $and: [{ "names.name": roomName }, { members: { $in: [user] } }],
  });

  if (!roomExists) res.status(400).send("Chat room doesn't exist!");
  else res.status(200).send(roomExists._id);
};
