const User = require("../models/user");

exports.getProfileImage = async (req, res) => {
    const username = req.params.username;

    const user = await User.findOne({ name: username });
    
    if(!user)
        res.status(400).send("User doesn't exist!");
    else {
        res.status(200).send(user.profileImage);
    }
};