
exports.postProfileImage = async (req, res) => {
    
    if (req.file === undefined) return res.send("you must select a file.");
    const imgUrl = `http://localhost:5000/api/image/${req.file.filename}`;
    return res.send(imgUrl);
};

exports.getProfileImage = async (req, res, next) => {
 
  const fileName = req.params.fileName;
 
  gfs.files.findOne({ filename: fileName }, (err, file) => {
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: "No file exists",
      });
    }

    if (file.contentType === "image/jpeg" || file.contentType === "image/png") {
      const readstream = gfs.createReadStream(file.filename);
      readstream.pipe(res);
    } else {
      res.status(200).json({
        success: true,
        file: files[0],
      });
    }
  });
};
