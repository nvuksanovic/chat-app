const ChatRoom = require("../models/chatRoom");
const Message = require("../models/message");

exports.postMessage = async (req, res, next) => {
  const message = req.body.message;
  console.log(message)
  const roomId = message.chatRoom._id;

  const newMessage = new Message({
    text: message.text,
    sender: message.sender,
    chatRoomId: roomId,
  });

  try {
    const savedMessage = await newMessage.save();
    res.status(200).json(savedMessage);
  } catch (err) {
    res.status(500).json(err);
  }
};

exports.getMessagesFromChatRoom = async (req, res, next) => {
    const chatRoomId = req.params.chatRoomId;

    const roomExists = await ChatRoom.findOne({_id: chatRoomId});
    if(!roomExists){
        res.status(400).send("This chat room doesn't exist!");
    }
    const result = await Message.find({chatRoomId: chatRoomId});
    if(result) res.status(200).json(result);
    else res.status(400).send("This chat room doesn't have any messages!");
};
