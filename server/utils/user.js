const users = [];

exports.userJoin = (id, username, room) => {
  const user = { id, username, room };
  users.push(user);
  console.log("JOIN ",users)
  return user;
};

exports.getUsersFromRoom = (room) => {
    console.log("GET ", users)
    return users.filter(user => user.room === room);
};
