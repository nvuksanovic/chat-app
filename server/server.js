const http = require("http");
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const Grid = require("gridfs-stream");

require("dotenv").config();

const socketIo = require("socket.io");

const authRoute = require("./routes/auth");
const chatRoomRoute = require("./routes/chatRoom");
const messageRoute = require("./routes/message");
const imageUploadRoute = require("./routes/imageUpload");
const userRoute = require("./routes/user");

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const MONGODB_URI = process.env.DB_CONNECT;

const store = new MongoDBStore({
  uri: MONGODB_URI,
  collection: "sessions",
});

app.use(
  session({
    secret: "my secret",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);

const server = http.createServer(app);

const io = socketIo(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
    credentials: true,
  },
});

require("./controllers/chat")(io);

const PORT = 5000 || process.env.PORT;

let gfs;

const conn = mongoose.connection;
conn.once("open", function () {
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection("photos");
});


app.use("/api", authRoute);
app.use("/api/chatroom", chatRoomRoute);
app.use("/api/message", messageRoute);
app.use("/api/image", imageUploadRoute);
app.use("/api/user", userRoute);

app.get("/api/image/:filename", async (req, res) => {
  try {
      const file = await gfs.files.findOne({ filename: {$regex : req.params.filename}});
      const readStream = gfs.createReadStream(file.filename);
      readStream.pipe(res);
  } catch (error) {
      res.send("not found");
  }
});

mongoose
  .connect(MONGODB_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then((res) => {
    server.listen(PORT, () => {
      console.log(`Listening on port ${PORT}`);
    });
  })
  .catch((err) => console.log(err));


